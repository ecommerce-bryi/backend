#!/bin/bash
echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@myproject.com', 'password')" | python manage.py shell

gunicorn --bind 0.0.0.0:8000 djackets_django.wsgi \
--name djangoapi \
--workers 3 \
--timeout 120