Django==3.2.8
django-cors-headers
djangorestframework
psycopg2
psycopg2-binary 
Pillow
gunicorn
djoser
stripe
boto3
django-storages
django-health-check 